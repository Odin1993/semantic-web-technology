/****************************************************************************
** Meta object code from reading C++ file 'qluaconsole.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qlua/qluaconsole.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluaconsole.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaConsole[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       2,   79, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   13,   12,   12, 0x05,
      52,   46,   12,   12, 0x05,
      73,   12,   12,   12, 0x05,
      88,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      99,   12,   12,   12, 0x0a,
     122,   12,   12,   12, 0x0a,
     172,  151,   12,   12, 0x0a,
     213,  206,   12,   12, 0x2a,
     249,  242,   12,   12, 0x0a,
     270,   12,   12,   12, 0x0a,
     286,   12,   12,   12, 0x0a,
     311,  306,   12,   12, 0x0a,
     336,   12,   12,   12, 0x0a,

 // properties: name, type, flags
     355,  350, 0x01095103,
     369,  350, 0x01095103,

       0        // eod
};

static const char qt_meta_stringdata_QLuaConsole[] = {
    "QLuaConsole\0\0output\0consoleOutput(QByteArray)\0"
    "input\0ttyInput(QByteArray)\0ttyEndOfFile()\0"
    "ttyBreak()\0setCaptureOutput(bool)\0"
    "setPrintCapturedOutput(bool)\0"
    "engine,breakStopsLua\0"
    "setQtLuaEngine(QtLuaEngine*,bool)\0"
    "engine\0setQtLuaEngine(QtLuaEngine*)\0"
    "prompt\0readLine(QByteArray)\0abortReadLine()\0"
    "redisplayReadLine()\0line\0"
    "addToHistory(QByteArray)\0drainOutput()\0"
    "bool\0captureOutput\0printCapturedOutput\0"
};

void QLuaConsole::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaConsole *_t = static_cast<QLuaConsole *>(_o);
        switch (_id) {
        case 0: _t->consoleOutput((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 1: _t->ttyInput((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 2: _t->ttyEndOfFile(); break;
        case 3: _t->ttyBreak(); break;
        case 4: _t->setCaptureOutput((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->setPrintCapturedOutput((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->setQtLuaEngine((*reinterpret_cast< QtLuaEngine*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 7: _t->setQtLuaEngine((*reinterpret_cast< QtLuaEngine*(*)>(_a[1]))); break;
        case 8: _t->readLine((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 9: _t->abortReadLine(); break;
        case 10: _t->redisplayReadLine(); break;
        case 11: _t->addToHistory((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 12: _t->drainOutput(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaConsole::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaConsole::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QLuaConsole,
      qt_meta_data_QLuaConsole, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaConsole::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaConsole::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaConsole::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaConsole))
        return static_cast<void*>(const_cast< QLuaConsole*>(this));
    return QObject::qt_metacast(_clname);
}

int QLuaConsole::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = captureOutput(); break;
        case 1: *reinterpret_cast< bool*>(_v) = printCapturedOutput(); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setCaptureOutput(*reinterpret_cast< bool*>(_v)); break;
        case 1: setPrintCapturedOutput(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QLuaConsole::consoleOutput(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QLuaConsole::ttyInput(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QLuaConsole::ttyEndOfFile()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void QLuaConsole::ttyBreak()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
