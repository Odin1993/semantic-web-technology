/****************************************************************************
** Meta object code from reading C++ file 'qtluaengine.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qtlua/qtluaengine.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qtluaengine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QtLuaEngine[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
      10,   89, // properties
       1,  119, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   13,   12,   12, 0x05,
      45,   37,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      70,   12,   12,   12, 0x0a,
      92,   12,   12,   12, 0x0a,
     113,   12,   12,   12, 0x0a,
     148,  140,  135,   12, 0x0a,
     159,   12,  135,   12, 0x2a,
     177,  166,  135,   12, 0x0a,
     190,   12,  135,   12, 0x2a,
     207,  199,  135,   12, 0x0a,
     231,  229,  135,   12, 0x2a,
     248,  199,  135,   12, 0x0a,
     267,  229,  135,   12, 0x2a,
     294,  229,  281,   12, 0x0a,
     315,  229,  281,   12, 0x0a,

 // properties: name, type, flags
     344,  333, 0x0c095001,
     373,  361, 0x0b095001,
     391,  135, 0x01095103,
     404,  135, 0x01095103,
     416,  135, 0x01095103,
     429,  135, 0x01095001,
      13,  447, 0x00095009,
     453,  135, 0x01095001,
     459,  135, 0x01095001,
     467,  135, 0x01095001,

 // enums: name, flags, count, data
     447, 0x0,    3,  123,

 // enum data: key, value
     474, uint(QtLuaEngine::Ready),
     480, uint(QtLuaEngine::Running),
     488, uint(QtLuaEngine::Paused),

       0        // eod
};

static const char qt_meta_stringdata_QtLuaEngine[] = {
    "QtLuaEngine\0\0state\0stateChanged(int)\0"
    "message\0errorMessage(QByteArray)\0"
    "setPrintResults(bool)\0setPrintErrors(bool)\0"
    "setPauseOnError(bool)\0bool\0nopause\0"
    "stop(bool)\0stop()\0nocontinue\0resume(bool)\0"
    "resume()\0s,async\0eval(QByteArray,bool)\0"
    "s\0eval(QByteArray)\0eval(QString,bool)\0"
    "eval(QString)\0QVariantList\0"
    "evaluate(QByteArray)\0evaluate(QString)\0"
    "QByteArray\0lastErrorMessage\0QStringList\0"
    "lastErrorLocation\0printResults\0"
    "printErrors\0pauseOnError\0runSignalHandlers\0"
    "State\0ready\0running\0paused\0Ready\0"
    "Running\0Paused\0"
};

void QtLuaEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QtLuaEngine *_t = static_cast<QtLuaEngine *>(_o);
        switch (_id) {
        case 0: _t->stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->errorMessage((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 2: _t->setPrintResults((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->setPrintErrors((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setPauseOnError((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: { bool _r = _t->stop((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->stop();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->resume((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: { bool _r = _t->resume();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: { bool _r = _t->eval((*reinterpret_cast< QByteArray(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 10: { bool _r = _t->eval((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 11: { bool _r = _t->eval((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->eval((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: { QVariantList _r = _t->evaluate((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QVariantList*>(_a[0]) = _r; }  break;
        case 14: { QVariantList _r = _t->evaluate((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QVariantList*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QtLuaEngine::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QtLuaEngine::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QtLuaEngine,
      qt_meta_data_QtLuaEngine, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QtLuaEngine::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QtLuaEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QtLuaEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QtLuaEngine))
        return static_cast<void*>(const_cast< QtLuaEngine*>(this));
    return QObject::qt_metacast(_clname);
}

int QtLuaEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QByteArray*>(_v) = lastErrorMessage(); break;
        case 1: *reinterpret_cast< QStringList*>(_v) = lastErrorLocation(); break;
        case 2: *reinterpret_cast< bool*>(_v) = printResults(); break;
        case 3: *reinterpret_cast< bool*>(_v) = printErrors(); break;
        case 4: *reinterpret_cast< bool*>(_v) = pauseOnError(); break;
        case 5: *reinterpret_cast< bool*>(_v) = runSignalHandlers(); break;
        case 6: *reinterpret_cast< State*>(_v) = state(); break;
        case 7: *reinterpret_cast< bool*>(_v) = isReady(); break;
        case 8: *reinterpret_cast< bool*>(_v) = isRunning(); break;
        case 9: *reinterpret_cast< bool*>(_v) = isPaused(); break;
        }
        _id -= 10;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 2: setPrintResults(*reinterpret_cast< bool*>(_v)); break;
        case 3: setPrintErrors(*reinterpret_cast< bool*>(_v)); break;
        case 4: setPauseOnError(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 10;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QtLuaEngine::stateChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QtLuaEngine::errorMessage(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
