# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/data/s1531565/torch/exe/qtlua/packages/qtcore/qtcore.cpp" "/data/s1531565/torch/exe/qtlua/build/packages/qtcore/CMakeFiles/libqtcore.dir/qtcore.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/data/s1531565/torch/install/include"
  "/usr/include/QtGui"
  "/usr/include/QtCore"
  "../packages/qtcore"
  "packages/qtcore"
  "../qtlua"
  "qtlua"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/data/s1531565/torch/exe/qtlua/build/qtlua/CMakeFiles/libqtlua.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
