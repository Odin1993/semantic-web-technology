/********************************************************************************
** Form generated from reading UI file 'qluareplacedialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QLUAREPLACEDIALOG_H
#define UI_QLUAREPLACEDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_QLuaReplaceDialog
{
public:
    QVBoxLayout *vboxLayout;
    QGridLayout *gridLayout;
    QLabel *findLabel;
    QLineEdit *findEdit;
    QLabel *replaceLabel;
    QLineEdit *replaceEdit;
    QHBoxLayout *hboxLayout;
    QCheckBox *searchBackwardsBox;
    QSpacerItem *spacerItem;
    QCheckBox *caseSensitiveBox;
    QSpacerItem *spacerItem1;
    QCheckBox *wholeWordsBox;
    QHBoxLayout *hboxLayout1;
    QSpacerItem *spacerItem2;
    QPushButton *replaceAllButton;
    QPushButton *replaceButton;
    QPushButton *findButton;
    QPushButton *closeButton;

    void setupUi(QDialog *QLuaReplaceDialog)
    {
        if (QLuaReplaceDialog->objectName().isEmpty())
            QLuaReplaceDialog->setObjectName(QString::fromUtf8("QLuaReplaceDialog"));
        QLuaReplaceDialog->resize(454, 150);
        vboxLayout = new QVBoxLayout(QLuaReplaceDialog);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        findLabel = new QLabel(QLuaReplaceDialog);
        findLabel->setObjectName(QString::fromUtf8("findLabel"));

        gridLayout->addWidget(findLabel, 0, 0, 1, 1);

        findEdit = new QLineEdit(QLuaReplaceDialog);
        findEdit->setObjectName(QString::fromUtf8("findEdit"));

        gridLayout->addWidget(findEdit, 0, 1, 1, 1);

        replaceLabel = new QLabel(QLuaReplaceDialog);
        replaceLabel->setObjectName(QString::fromUtf8("replaceLabel"));

        gridLayout->addWidget(replaceLabel, 1, 0, 1, 1);

        replaceEdit = new QLineEdit(QLuaReplaceDialog);
        replaceEdit->setObjectName(QString::fromUtf8("replaceEdit"));

        gridLayout->addWidget(replaceEdit, 1, 1, 1, 1);


        vboxLayout->addLayout(gridLayout);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        searchBackwardsBox = new QCheckBox(QLuaReplaceDialog);
        searchBackwardsBox->setObjectName(QString::fromUtf8("searchBackwardsBox"));

        hboxLayout->addWidget(searchBackwardsBox);

        spacerItem = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        caseSensitiveBox = new QCheckBox(QLuaReplaceDialog);
        caseSensitiveBox->setObjectName(QString::fromUtf8("caseSensitiveBox"));

        hboxLayout->addWidget(caseSensitiveBox);

        spacerItem1 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem1);

        wholeWordsBox = new QCheckBox(QLuaReplaceDialog);
        wholeWordsBox->setObjectName(QString::fromUtf8("wholeWordsBox"));

        hboxLayout->addWidget(wholeWordsBox);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        spacerItem2 = new QSpacerItem(20, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem2);

        replaceAllButton = new QPushButton(QLuaReplaceDialog);
        replaceAllButton->setObjectName(QString::fromUtf8("replaceAllButton"));
        replaceAllButton->setAutoDefault(false);

        hboxLayout1->addWidget(replaceAllButton);

        replaceButton = new QPushButton(QLuaReplaceDialog);
        replaceButton->setObjectName(QString::fromUtf8("replaceButton"));
        replaceButton->setAutoDefault(true);

        hboxLayout1->addWidget(replaceButton);

        findButton = new QPushButton(QLuaReplaceDialog);
        findButton->setObjectName(QString::fromUtf8("findButton"));
        findButton->setDefault(true);

        hboxLayout1->addWidget(findButton);

        closeButton = new QPushButton(QLuaReplaceDialog);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        closeButton->setAutoDefault(false);

        hboxLayout1->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout1);

#ifndef QT_NO_SHORTCUT
        findLabel->setBuddy(findEdit);
        replaceLabel->setBuddy(replaceEdit);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(findEdit, replaceEdit);
        QWidget::setTabOrder(replaceEdit, searchBackwardsBox);
        QWidget::setTabOrder(searchBackwardsBox, caseSensitiveBox);
        QWidget::setTabOrder(caseSensitiveBox, wholeWordsBox);
        QWidget::setTabOrder(wholeWordsBox, findButton);
        QWidget::setTabOrder(findButton, replaceButton);
        QWidget::setTabOrder(replaceButton, replaceAllButton);
        QWidget::setTabOrder(replaceAllButton, closeButton);

        retranslateUi(QLuaReplaceDialog);
        QObject::connect(closeButton, SIGNAL(clicked()), QLuaReplaceDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(QLuaReplaceDialog);
    } // setupUi

    void retranslateUi(QDialog *QLuaReplaceDialog)
    {
        QLuaReplaceDialog->setWindowTitle(QApplication::translate("QLuaReplaceDialog", "Replace", 0, QApplication::UnicodeUTF8));
        findLabel->setText(QApplication::translate("QLuaReplaceDialog", "Replace:", 0, QApplication::UnicodeUTF8));
        replaceLabel->setText(QApplication::translate("QLuaReplaceDialog", "with:", 0, QApplication::UnicodeUTF8));
        searchBackwardsBox->setText(QApplication::translate("QLuaReplaceDialog", "Search &backwards", 0, QApplication::UnicodeUTF8));
        caseSensitiveBox->setText(QApplication::translate("QLuaReplaceDialog", "&Case sensitive", 0, QApplication::UnicodeUTF8));
        wholeWordsBox->setText(QApplication::translate("QLuaReplaceDialog", "&Whole words", 0, QApplication::UnicodeUTF8));
        replaceAllButton->setText(QApplication::translate("QLuaReplaceDialog", "Replace &All", 0, QApplication::UnicodeUTF8));
        replaceButton->setText(QApplication::translate("QLuaReplaceDialog", "&Replace", 0, QApplication::UnicodeUTF8));
        findButton->setText(QApplication::translate("QLuaReplaceDialog", "&Next", 0, QApplication::UnicodeUTF8));
        closeButton->setText(QApplication::translate("QLuaReplaceDialog", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class QLuaReplaceDialog: public Ui_QLuaReplaceDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QLUAREPLACEDIALOG_H
