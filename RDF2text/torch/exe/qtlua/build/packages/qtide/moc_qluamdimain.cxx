/****************************************************************************
** Meta object code from reading C++ file 'qluamdimain.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtide/qluamdimain.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluamdimain.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaMdiMain[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       2,   69, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   18,   13,   12, 0x0a,
      39,   18,   13,   12, 0x0a,
      55,   12,   12,   12, 0x0a,
      68,   66,   12,   12, 0x0a,
      87,   85,   12,   12, 0x0a,
     114,   12,   12,   12, 0x0a,

 // methods: signature, parameters, type, tag, flags
     132,   12,  122,   12, 0x02,
     142,   18,   13,   12, 0x02,
     161,   12,   13,   12, 0x02,
     184,   12,  173,   12, 0x02,
     207,   12,  198,   12, 0x02,

 // properties: name, type, flags
     222,   13, 0x01095103,
     230,  173, 0x0c095103,

       0        // eod
};

static const char qt_meta_stringdata_QLuaMdiMain[] = {
    "QLuaMdiMain\0\0bool\0w\0activate(QWidget*)\0"
    "adopt(QWidget*)\0adoptAll()\0b\0"
    "setTabMode(bool)\0c\0setClientClass(QByteArray)\0"
    "doNew()\0QMdiArea*\0mdiArea()\0"
    "isActive(QWidget*)\0isTabMode()\0"
    "QByteArray\0clientClass()\0QWidget*\0"
    "activeWindow()\0tabMode\0clientClass\0"
};

void QLuaMdiMain::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaMdiMain *_t = static_cast<QLuaMdiMain *>(_o);
        switch (_id) {
        case 0: { bool _r = _t->activate((*reinterpret_cast< QWidget*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 1: { bool _r = _t->adopt((*reinterpret_cast< QWidget*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: _t->adoptAll(); break;
        case 3: _t->setTabMode((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setClientClass((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 5: _t->doNew(); break;
        case 6: { QMdiArea* _r = _t->mdiArea();
            if (_a[0]) *reinterpret_cast< QMdiArea**>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->isActive((*reinterpret_cast< QWidget*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: { bool _r = _t->isTabMode();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: { QByteArray _r = _t->clientClass();
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        case 10: { QWidget* _r = _t->activeWindow();
            if (_a[0]) *reinterpret_cast< QWidget**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaMdiMain::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaMdiMain::staticMetaObject = {
    { &QLuaMainWindow::staticMetaObject, qt_meta_stringdata_QLuaMdiMain,
      qt_meta_data_QLuaMdiMain, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaMdiMain::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaMdiMain::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaMdiMain::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaMdiMain))
        return static_cast<void*>(const_cast< QLuaMdiMain*>(this));
    return QLuaMainWindow::qt_metacast(_clname);
}

int QLuaMdiMain::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLuaMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = isTabMode(); break;
        case 1: *reinterpret_cast< QByteArray*>(_v) = clientClass(); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setTabMode(*reinterpret_cast< bool*>(_v)); break;
        case 1: setClientClass(*reinterpret_cast< QByteArray*>(_v)); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
