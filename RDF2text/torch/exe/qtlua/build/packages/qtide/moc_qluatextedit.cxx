/****************************************************************************
** Meta object code from reading C++ file 'qluatextedit.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtide/qluatextedit.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluatextedit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaTextEdit[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       8,  119, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      34,   32,   13,   13, 0x0a,
      59,   32,   13,   13, 0x0a,
      81,   32,   13,   13, 0x0a,
     101,   32,   13,   13, 0x0a,
     124,   32,   13,   13, 0x0a,
     143,   32,   13,   13, 0x0a,
     164,  162,   13,   13, 0x0a,
     185,  180,   13,   13, 0x0a,
     224,  212,  207,   13, 0x0a,
     264,   13,  207,   13, 0x2a,
     287,  280,  207,   13, 0x0a,
     316,  310,  207,   13, 0x0a,
     334,  310,  207,   13, 0x0a,
     360,  353,   13,   13, 0x0a,
     374,   13,   13,   13, 0x0a,

 // methods: signature, parameters, type, tag, flags
     406,   13,  388,   13, 0x02,
     428,   13,  419,   13, 0x02,
     445,   13,  419,   13, 0x02,
     465,   13,  419,   13, 0x02,
     489,  482,   13,   13, 0x02,

 // properties: name, type, flags
     513,  207, 0x01095103,
     529,  207, 0x01095103,
     542,  207, 0x01095103,
     553,  207, 0x01095103,
     567,  207, 0x01095103,
     577,  207, 0x01095103,
     591,  587, 0x02095103,
     605,  599, 0x15095103,

       0        // eod
};

static const char qt_meta_stringdata_QLuaTextEdit[] = {
    "QLuaTextEdit\0\0settingsChanged()\0b\0"
    "setShowLineNumbers(bool)\0setAutoComplete(bool)\0"
    "setAutoIndent(bool)\0setAutoHighlight(bool)\0"
    "setAutoMatch(bool)\0setTabExpand(bool)\0"
    "s\0setTabSize(int)\0size\0setSizeInChars(QSize)\0"
    "bool\0modeFactory\0"
    "setEditorMode(QLuaTextEditModeFactory*)\0"
    "setEditorMode()\0suffix\0setEditorMode(QString)\0"
    "fname\0readFile(QString)\0writeFile(QString)\0"
    "lineno\0showLine(int)\0reHighlight()\0"
    "QLuaTextEditMode*\0editorMode()\0QDialog*\0"
    "makeFindDialog()\0makeReplaceDialog()\0"
    "makeGotoDialog()\0dialog\0prepareDialog(QDialog*)\0"
    "showLineNumbers\0autoComplete\0autoIndent\0"
    "autoHighlight\0autoMatch\0tabExpand\0int\0"
    "tabSize\0QSize\0sizeInChars\0"
};

void QLuaTextEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaTextEdit *_t = static_cast<QLuaTextEdit *>(_o);
        switch (_id) {
        case 0: _t->settingsChanged(); break;
        case 1: _t->setShowLineNumbers((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->setAutoComplete((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->setAutoIndent((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setAutoHighlight((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->setAutoMatch((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->setTabExpand((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->setTabSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->setSizeInChars((*reinterpret_cast< QSize(*)>(_a[1]))); break;
        case 9: { bool _r = _t->setEditorMode((*reinterpret_cast< QLuaTextEditModeFactory*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 10: { bool _r = _t->setEditorMode();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 11: { bool _r = _t->setEditorMode((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->readFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: { bool _r = _t->writeFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 14: _t->showLine((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->reHighlight(); break;
        case 16: { QLuaTextEditMode* _r = _t->editorMode();
            if (_a[0]) *reinterpret_cast< QLuaTextEditMode**>(_a[0]) = _r; }  break;
        case 17: { QDialog* _r = _t->makeFindDialog();
            if (_a[0]) *reinterpret_cast< QDialog**>(_a[0]) = _r; }  break;
        case 18: { QDialog* _r = _t->makeReplaceDialog();
            if (_a[0]) *reinterpret_cast< QDialog**>(_a[0]) = _r; }  break;
        case 19: { QDialog* _r = _t->makeGotoDialog();
            if (_a[0]) *reinterpret_cast< QDialog**>(_a[0]) = _r; }  break;
        case 20: _t->prepareDialog((*reinterpret_cast< QDialog*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaTextEdit::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaTextEdit::staticMetaObject = {
    { &QPlainTextEdit::staticMetaObject, qt_meta_stringdata_QLuaTextEdit,
      qt_meta_data_QLuaTextEdit, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaTextEdit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaTextEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaTextEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaTextEdit))
        return static_cast<void*>(const_cast< QLuaTextEdit*>(this));
    return QPlainTextEdit::qt_metacast(_clname);
}

int QLuaTextEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QPlainTextEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = showLineNumbers(); break;
        case 1: *reinterpret_cast< bool*>(_v) = autoComplete(); break;
        case 2: *reinterpret_cast< bool*>(_v) = autoIndent(); break;
        case 3: *reinterpret_cast< bool*>(_v) = autoHighlight(); break;
        case 4: *reinterpret_cast< bool*>(_v) = autoMatch(); break;
        case 5: *reinterpret_cast< bool*>(_v) = tabExpand(); break;
        case 6: *reinterpret_cast< int*>(_v) = tabSize(); break;
        case 7: *reinterpret_cast< QSize*>(_v) = sizeInChars(); break;
        }
        _id -= 8;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setShowLineNumbers(*reinterpret_cast< bool*>(_v)); break;
        case 1: setAutoComplete(*reinterpret_cast< bool*>(_v)); break;
        case 2: setAutoIndent(*reinterpret_cast< bool*>(_v)); break;
        case 3: setAutoHighlight(*reinterpret_cast< bool*>(_v)); break;
        case 4: setAutoMatch(*reinterpret_cast< bool*>(_v)); break;
        case 5: setTabExpand(*reinterpret_cast< bool*>(_v)); break;
        case 6: setTabSize(*reinterpret_cast< int*>(_v)); break;
        case 7: setSizeInChars(*reinterpret_cast< QSize*>(_v)); break;
        }
        _id -= 8;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QLuaTextEdit::settingsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_QLuaTextEditMode[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   17,   18,   17, 0x0a,
      33,   17,   18,   17, 0x0a,
      41,   17,   18,   17, 0x0a,
      51,   17,   18,   17, 0x0a,
      63,   17,   18,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QLuaTextEditMode[] = {
    "QLuaTextEditMode\0\0bool\0doEnter()\0"
    "doTab()\0doMatch()\0doBalance()\0"
    "doComplete()\0"
};

void QLuaTextEditMode::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaTextEditMode *_t = static_cast<QLuaTextEditMode *>(_o);
        switch (_id) {
        case 0: { bool _r = _t->doEnter();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 1: { bool _r = _t->doTab();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: { bool _r = _t->doMatch();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 3: { bool _r = _t->doBalance();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: { bool _r = _t->doComplete();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaTextEditMode::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaTextEditMode::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QLuaTextEditMode,
      qt_meta_data_QLuaTextEditMode, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaTextEditMode::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaTextEditMode::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaTextEditMode::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaTextEditMode))
        return static_cast<void*>(const_cast< QLuaTextEditMode*>(this));
    return QObject::qt_metacast(_clname);
}

int QLuaTextEditMode::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
