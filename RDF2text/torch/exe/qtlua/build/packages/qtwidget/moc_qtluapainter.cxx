/****************************************************************************
** Meta object code from reading C++ file 'qtluapainter.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtwidget/qtluapainter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qtluapainter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QtLuaPainter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
      15,   74, // properties
       4,  119, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      25,   13,   13,   13, 0x0a,

 // methods: signature, parameters, type, tag, flags
      42,   13,   35,   13, 0x02,
      58,   13,   50,   13, 0x02,
      76,   13,   67,   13, 0x02,
      94,   13,   85,   13, 0x02,
     117,   13,  103,   13, 0x02,
     136,   13,  126,   13, 0x02,
     156,   13,  146,   13, 0x02,
     172,   13,  166,   13, 0x02,
     185,   13,  179,   13, 0x02,
     192,   13,   13,   13, 0x02,

 // properties: name, type, flags
     205,  200, 0x4d095003,
     216,  209, 0x42095003,
     230,  222, 0x1a095003,
     249,  236, 0x0009500b,
     254,  236, 0x0009500b,
     269,  263, 0x40095003,
     285,  274, 0x51095003,
     292,  209, 0x42095003,
     319,  303, 0x0009500b,
     347,  335, 0x0009500b,
     369,  359, 0x0009500b,
     387,  379, 0x0a095003,
     402,  398, 0x02095001,
     408,  398, 0x02095001,
     415,  398, 0x02095001,

 // enums: name, flags, count, data
     359, 0x0,    2,  135,
     303, 0x0,   24,  139,
     335, 0x1,    4,  187,
     421, 0x1,   14,  195,

 // enum data: key, value
     431, uint(QtLuaPainter::Degrees),
     439, uint(QtLuaPainter::Radians),
     447, uint(QtLuaPainter::SourceOver),
     458, uint(QtLuaPainter::DestinationOver),
     474, uint(QtLuaPainter::Clear),
     480, uint(QtLuaPainter::Source),
     487, uint(QtLuaPainter::Destination),
     499, uint(QtLuaPainter::SourceIn),
     508, uint(QtLuaPainter::DestinationIn),
     522, uint(QtLuaPainter::SourceOut),
     532, uint(QtLuaPainter::DestinationOut),
     547, uint(QtLuaPainter::SourceAtop),
     558, uint(QtLuaPainter::DestinationAtop),
     574, uint(QtLuaPainter::Xor),
     578, uint(QtLuaPainter::Plus),
     583, uint(QtLuaPainter::Multiply),
     592, uint(QtLuaPainter::Screen),
     599, uint(QtLuaPainter::Overlay),
     607, uint(QtLuaPainter::Darken),
     614, uint(QtLuaPainter::Lighten),
     622, uint(QtLuaPainter::ColorDodge),
     633, uint(QtLuaPainter::ColorBurn),
     643, uint(QtLuaPainter::HardLight),
     653, uint(QtLuaPainter::SoftLight),
     663, uint(QtLuaPainter::Difference),
     674, uint(QtLuaPainter::Exclusion),
     684, uint(QtLuaPainter::Antialiasing),
     697, uint(QtLuaPainter::TextAntialiasing),
     714, uint(QtLuaPainter::SmoothPixmapTransform),
     736, uint(QtLuaPainter::HighQualityAntialiasing),
     760, uint(QtLuaPainter::AlignLeft),
     770, uint(QtLuaPainter::AlignRight),
     781, uint(QtLuaPainter::AlignHCenter),
     794, uint(QtLuaPainter::AlignJustify),
     807, uint(QtLuaPainter::AlignTop),
     816, uint(QtLuaPainter::AlignBottom),
     828, uint(QtLuaPainter::AlignVCenter),
     841, uint(QtLuaPainter::AlignCenter),
     853, uint(QtLuaPainter::TextSingleLine),
     868, uint(QtLuaPainter::TextExpandTabs),
     883, uint(QtLuaPainter::TextShowMnemonic),
     900, uint(QtLuaPainter::TextWordWrap),
     913, uint(QtLuaPainter::TextRich),
     922, uint(QtLuaPainter::RichText),

       0        // eod
};

static const char qt_meta_stringdata_QtLuaPainter[] = {
    "QtLuaPainter\0\0showpage()\0refresh()\0"
    "QImage\0image()\0QPixmap\0pixmap()\0"
    "QWidget*\0widget()\0QObject*\0object()\0"
    "QPaintDevice*\0device()\0QPrinter*\0"
    "printer()\0QPainter*\0painter()\0QRect\0"
    "rect()\0QSize\0size()\0close()\0QPen\0pen\0"
    "QBrush\0brush\0QPointF\0point\0QPainterPath\0"
    "path\0clippath\0QFont\0font\0QTransform\0"
    "matrix\0background\0CompositionMode\0"
    "compositionmode\0RenderHints\0renderhints\0"
    "AngleUnit\0angleUnit\0QString\0styleSheet\0"
    "int\0width\0height\0depth\0TextFlags\0"
    "Degrees\0Radians\0SourceOver\0DestinationOver\0"
    "Clear\0Source\0Destination\0SourceIn\0"
    "DestinationIn\0SourceOut\0DestinationOut\0"
    "SourceAtop\0DestinationAtop\0Xor\0Plus\0"
    "Multiply\0Screen\0Overlay\0Darken\0Lighten\0"
    "ColorDodge\0ColorBurn\0HardLight\0SoftLight\0"
    "Difference\0Exclusion\0Antialiasing\0"
    "TextAntialiasing\0SmoothPixmapTransform\0"
    "HighQualityAntialiasing\0AlignLeft\0"
    "AlignRight\0AlignHCenter\0AlignJustify\0"
    "AlignTop\0AlignBottom\0AlignVCenter\0"
    "AlignCenter\0TextSingleLine\0TextExpandTabs\0"
    "TextShowMnemonic\0TextWordWrap\0TextRich\0"
    "RichText\0"
};

void QtLuaPainter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QtLuaPainter *_t = static_cast<QtLuaPainter *>(_o);
        switch (_id) {
        case 0: _t->showpage(); break;
        case 1: _t->refresh(); break;
        case 2: { QImage _r = _t->image();
            if (_a[0]) *reinterpret_cast< QImage*>(_a[0]) = _r; }  break;
        case 3: { QPixmap _r = _t->pixmap();
            if (_a[0]) *reinterpret_cast< QPixmap*>(_a[0]) = _r; }  break;
        case 4: { QWidget* _r = _t->widget();
            if (_a[0]) *reinterpret_cast< QWidget**>(_a[0]) = _r; }  break;
        case 5: { QObject* _r = _t->object();
            if (_a[0]) *reinterpret_cast< QObject**>(_a[0]) = _r; }  break;
        case 6: { QPaintDevice* _r = _t->device();
            if (_a[0]) *reinterpret_cast< QPaintDevice**>(_a[0]) = _r; }  break;
        case 7: { QPrinter* _r = _t->printer();
            if (_a[0]) *reinterpret_cast< QPrinter**>(_a[0]) = _r; }  break;
        case 8: { QPainter* _r = _t->painter();
            if (_a[0]) *reinterpret_cast< QPainter**>(_a[0]) = _r; }  break;
        case 9: { QRect _r = _t->rect();
            if (_a[0]) *reinterpret_cast< QRect*>(_a[0]) = _r; }  break;
        case 10: { QSize _r = _t->size();
            if (_a[0]) *reinterpret_cast< QSize*>(_a[0]) = _r; }  break;
        case 11: _t->close(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QtLuaPainter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QtLuaPainter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QtLuaPainter,
      qt_meta_data_QtLuaPainter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QtLuaPainter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QtLuaPainter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QtLuaPainter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QtLuaPainter))
        return static_cast<void*>(const_cast< QtLuaPainter*>(this));
    return QObject::qt_metacast(_clname);
}

int QtLuaPainter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QPen*>(_v) = currentpen(); break;
        case 1: *reinterpret_cast< QBrush*>(_v) = currentbrush(); break;
        case 2: *reinterpret_cast< QPointF*>(_v) = currentpoint(); break;
        case 3: *reinterpret_cast< QPainterPath*>(_v) = currentpath(); break;
        case 4: *reinterpret_cast< QPainterPath*>(_v) = currentclip(); break;
        case 5: *reinterpret_cast< QFont*>(_v) = currentfont(); break;
        case 6: *reinterpret_cast< QTransform*>(_v) = currentmatrix(); break;
        case 7: *reinterpret_cast< QBrush*>(_v) = currentbackground(); break;
        case 8: *reinterpret_cast< CompositionMode*>(_v) = currentmode(); break;
        case 9: *reinterpret_cast<int*>(_v) = QFlag(currenthints()); break;
        case 10: *reinterpret_cast< AngleUnit*>(_v) = currentangleunit(); break;
        case 11: *reinterpret_cast< QString*>(_v) = currentstylesheet(); break;
        case 12: *reinterpret_cast< int*>(_v) = width(); break;
        case 13: *reinterpret_cast< int*>(_v) = height(); break;
        case 14: *reinterpret_cast< int*>(_v) = depth(); break;
        }
        _id -= 15;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setpen(*reinterpret_cast< QPen*>(_v)); break;
        case 1: setbrush(*reinterpret_cast< QBrush*>(_v)); break;
        case 2: setpoint(*reinterpret_cast< QPointF*>(_v)); break;
        case 3: setpath(*reinterpret_cast< QPainterPath*>(_v)); break;
        case 4: setclip(*reinterpret_cast< QPainterPath*>(_v)); break;
        case 5: setfont(*reinterpret_cast< QFont*>(_v)); break;
        case 6: setmatrix(*reinterpret_cast< QTransform*>(_v)); break;
        case 7: setbackground(*reinterpret_cast< QBrush*>(_v)); break;
        case 8: setmode(*reinterpret_cast< CompositionMode*>(_v)); break;
        case 9: sethints(QFlag(*reinterpret_cast<int*>(_v))); break;
        case 10: setangleunit(*reinterpret_cast< AngleUnit*>(_v)); break;
        case 11: setstylesheet(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 15;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 15;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
