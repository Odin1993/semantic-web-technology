# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.6

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /apps/sandybridge/software/CMake/3.6.1-foss-2016a/bin/cmake

# The command to remove a file.
RM = /apps/sandybridge/software/CMake/3.6.1-foss-2016a/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /data/s1531565/torch/pkg/torch

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /data/s1531565/torch/pkg/torch/build

# Include any dependencies generated for this target.
include CMakeFiles/torch.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/torch.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/torch.dir/flags.make

TensorMath.c: ../TensorMath.lua
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Generating TensorMath.c"
	cd /data/s1531565/torch/pkg/torch && /data/s1531565/torch/install/bin/lua /data/s1531565/torch/pkg/torch/TensorMath.lua /data/s1531565/torch/pkg/torch/build/TensorMath.c

random.c: ../random.lua
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Generating random.c"
	cd /data/s1531565/torch/pkg/torch && /data/s1531565/torch/install/bin/lua /data/s1531565/torch/pkg/torch/random.lua /data/s1531565/torch/pkg/torch/build/random.c

CMakeFiles/torch.dir/DiskFile.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/DiskFile.c.o: ../DiskFile.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building C object CMakeFiles/torch.dir/DiskFile.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/DiskFile.c.o   -c /data/s1531565/torch/pkg/torch/DiskFile.c

CMakeFiles/torch.dir/DiskFile.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/DiskFile.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/DiskFile.c > CMakeFiles/torch.dir/DiskFile.c.i

CMakeFiles/torch.dir/DiskFile.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/DiskFile.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/DiskFile.c -o CMakeFiles/torch.dir/DiskFile.c.s

CMakeFiles/torch.dir/DiskFile.c.o.requires:

.PHONY : CMakeFiles/torch.dir/DiskFile.c.o.requires

CMakeFiles/torch.dir/DiskFile.c.o.provides: CMakeFiles/torch.dir/DiskFile.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/DiskFile.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/DiskFile.c.o.provides

CMakeFiles/torch.dir/DiskFile.c.o.provides.build: CMakeFiles/torch.dir/DiskFile.c.o


CMakeFiles/torch.dir/File.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/File.c.o: ../File.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Building C object CMakeFiles/torch.dir/File.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/File.c.o   -c /data/s1531565/torch/pkg/torch/File.c

CMakeFiles/torch.dir/File.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/File.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/File.c > CMakeFiles/torch.dir/File.c.i

CMakeFiles/torch.dir/File.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/File.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/File.c -o CMakeFiles/torch.dir/File.c.s

CMakeFiles/torch.dir/File.c.o.requires:

.PHONY : CMakeFiles/torch.dir/File.c.o.requires

CMakeFiles/torch.dir/File.c.o.provides: CMakeFiles/torch.dir/File.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/File.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/File.c.o.provides

CMakeFiles/torch.dir/File.c.o.provides.build: CMakeFiles/torch.dir/File.c.o


CMakeFiles/torch.dir/MemoryFile.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/MemoryFile.c.o: ../MemoryFile.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Building C object CMakeFiles/torch.dir/MemoryFile.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/MemoryFile.c.o   -c /data/s1531565/torch/pkg/torch/MemoryFile.c

CMakeFiles/torch.dir/MemoryFile.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/MemoryFile.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/MemoryFile.c > CMakeFiles/torch.dir/MemoryFile.c.i

CMakeFiles/torch.dir/MemoryFile.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/MemoryFile.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/MemoryFile.c -o CMakeFiles/torch.dir/MemoryFile.c.s

CMakeFiles/torch.dir/MemoryFile.c.o.requires:

.PHONY : CMakeFiles/torch.dir/MemoryFile.c.o.requires

CMakeFiles/torch.dir/MemoryFile.c.o.provides: CMakeFiles/torch.dir/MemoryFile.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/MemoryFile.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/MemoryFile.c.o.provides

CMakeFiles/torch.dir/MemoryFile.c.o.provides.build: CMakeFiles/torch.dir/MemoryFile.c.o


CMakeFiles/torch.dir/PipeFile.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/PipeFile.c.o: ../PipeFile.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Building C object CMakeFiles/torch.dir/PipeFile.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/PipeFile.c.o   -c /data/s1531565/torch/pkg/torch/PipeFile.c

CMakeFiles/torch.dir/PipeFile.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/PipeFile.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/PipeFile.c > CMakeFiles/torch.dir/PipeFile.c.i

CMakeFiles/torch.dir/PipeFile.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/PipeFile.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/PipeFile.c -o CMakeFiles/torch.dir/PipeFile.c.s

CMakeFiles/torch.dir/PipeFile.c.o.requires:

.PHONY : CMakeFiles/torch.dir/PipeFile.c.o.requires

CMakeFiles/torch.dir/PipeFile.c.o.provides: CMakeFiles/torch.dir/PipeFile.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/PipeFile.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/PipeFile.c.o.provides

CMakeFiles/torch.dir/PipeFile.c.o.provides.build: CMakeFiles/torch.dir/PipeFile.c.o


CMakeFiles/torch.dir/Storage.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/Storage.c.o: ../Storage.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "Building C object CMakeFiles/torch.dir/Storage.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/Storage.c.o   -c /data/s1531565/torch/pkg/torch/Storage.c

CMakeFiles/torch.dir/Storage.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/Storage.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/Storage.c > CMakeFiles/torch.dir/Storage.c.i

CMakeFiles/torch.dir/Storage.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/Storage.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/Storage.c -o CMakeFiles/torch.dir/Storage.c.s

CMakeFiles/torch.dir/Storage.c.o.requires:

.PHONY : CMakeFiles/torch.dir/Storage.c.o.requires

CMakeFiles/torch.dir/Storage.c.o.provides: CMakeFiles/torch.dir/Storage.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/Storage.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/Storage.c.o.provides

CMakeFiles/torch.dir/Storage.c.o.provides.build: CMakeFiles/torch.dir/Storage.c.o


CMakeFiles/torch.dir/Tensor.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/Tensor.c.o: ../Tensor.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_8) "Building C object CMakeFiles/torch.dir/Tensor.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/Tensor.c.o   -c /data/s1531565/torch/pkg/torch/Tensor.c

CMakeFiles/torch.dir/Tensor.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/Tensor.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/Tensor.c > CMakeFiles/torch.dir/Tensor.c.i

CMakeFiles/torch.dir/Tensor.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/Tensor.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/Tensor.c -o CMakeFiles/torch.dir/Tensor.c.s

CMakeFiles/torch.dir/Tensor.c.o.requires:

.PHONY : CMakeFiles/torch.dir/Tensor.c.o.requires

CMakeFiles/torch.dir/Tensor.c.o.provides: CMakeFiles/torch.dir/Tensor.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/Tensor.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/Tensor.c.o.provides

CMakeFiles/torch.dir/Tensor.c.o.provides.build: CMakeFiles/torch.dir/Tensor.c.o


CMakeFiles/torch.dir/Timer.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/Timer.c.o: ../Timer.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_9) "Building C object CMakeFiles/torch.dir/Timer.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/Timer.c.o   -c /data/s1531565/torch/pkg/torch/Timer.c

CMakeFiles/torch.dir/Timer.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/Timer.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/Timer.c > CMakeFiles/torch.dir/Timer.c.i

CMakeFiles/torch.dir/Timer.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/Timer.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/Timer.c -o CMakeFiles/torch.dir/Timer.c.s

CMakeFiles/torch.dir/Timer.c.o.requires:

.PHONY : CMakeFiles/torch.dir/Timer.c.o.requires

CMakeFiles/torch.dir/Timer.c.o.provides: CMakeFiles/torch.dir/Timer.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/Timer.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/Timer.c.o.provides

CMakeFiles/torch.dir/Timer.c.o.provides.build: CMakeFiles/torch.dir/Timer.c.o


CMakeFiles/torch.dir/utils.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/utils.c.o: ../utils.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_10) "Building C object CMakeFiles/torch.dir/utils.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/utils.c.o   -c /data/s1531565/torch/pkg/torch/utils.c

CMakeFiles/torch.dir/utils.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/utils.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/utils.c > CMakeFiles/torch.dir/utils.c.i

CMakeFiles/torch.dir/utils.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/utils.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/utils.c -o CMakeFiles/torch.dir/utils.c.s

CMakeFiles/torch.dir/utils.c.o.requires:

.PHONY : CMakeFiles/torch.dir/utils.c.o.requires

CMakeFiles/torch.dir/utils.c.o.provides: CMakeFiles/torch.dir/utils.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/utils.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/utils.c.o.provides

CMakeFiles/torch.dir/utils.c.o.provides.build: CMakeFiles/torch.dir/utils.c.o


CMakeFiles/torch.dir/init.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/init.c.o: ../init.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_11) "Building C object CMakeFiles/torch.dir/init.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/init.c.o   -c /data/s1531565/torch/pkg/torch/init.c

CMakeFiles/torch.dir/init.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/init.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/init.c > CMakeFiles/torch.dir/init.c.i

CMakeFiles/torch.dir/init.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/init.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/init.c -o CMakeFiles/torch.dir/init.c.s

CMakeFiles/torch.dir/init.c.o.requires:

.PHONY : CMakeFiles/torch.dir/init.c.o.requires

CMakeFiles/torch.dir/init.c.o.provides: CMakeFiles/torch.dir/init.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/init.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/init.c.o.provides

CMakeFiles/torch.dir/init.c.o.provides.build: CMakeFiles/torch.dir/init.c.o


CMakeFiles/torch.dir/TensorOperator.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/TensorOperator.c.o: ../TensorOperator.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_12) "Building C object CMakeFiles/torch.dir/TensorOperator.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/TensorOperator.c.o   -c /data/s1531565/torch/pkg/torch/TensorOperator.c

CMakeFiles/torch.dir/TensorOperator.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/TensorOperator.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/TensorOperator.c > CMakeFiles/torch.dir/TensorOperator.c.i

CMakeFiles/torch.dir/TensorOperator.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/TensorOperator.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/TensorOperator.c -o CMakeFiles/torch.dir/TensorOperator.c.s

CMakeFiles/torch.dir/TensorOperator.c.o.requires:

.PHONY : CMakeFiles/torch.dir/TensorOperator.c.o.requires

CMakeFiles/torch.dir/TensorOperator.c.o.provides: CMakeFiles/torch.dir/TensorOperator.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/TensorOperator.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/TensorOperator.c.o.provides

CMakeFiles/torch.dir/TensorOperator.c.o.provides.build: CMakeFiles/torch.dir/TensorOperator.c.o


CMakeFiles/torch.dir/TensorMath.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/TensorMath.c.o: TensorMath.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_13) "Building C object CMakeFiles/torch.dir/TensorMath.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/TensorMath.c.o   -c /data/s1531565/torch/pkg/torch/build/TensorMath.c

CMakeFiles/torch.dir/TensorMath.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/TensorMath.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/build/TensorMath.c > CMakeFiles/torch.dir/TensorMath.c.i

CMakeFiles/torch.dir/TensorMath.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/TensorMath.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/build/TensorMath.c -o CMakeFiles/torch.dir/TensorMath.c.s

CMakeFiles/torch.dir/TensorMath.c.o.requires:

.PHONY : CMakeFiles/torch.dir/TensorMath.c.o.requires

CMakeFiles/torch.dir/TensorMath.c.o.provides: CMakeFiles/torch.dir/TensorMath.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/TensorMath.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/TensorMath.c.o.provides

CMakeFiles/torch.dir/TensorMath.c.o.provides.build: CMakeFiles/torch.dir/TensorMath.c.o


CMakeFiles/torch.dir/random.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/random.c.o: random.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_14) "Building C object CMakeFiles/torch.dir/random.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/random.c.o   -c /data/s1531565/torch/pkg/torch/build/random.c

CMakeFiles/torch.dir/random.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/random.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/build/random.c > CMakeFiles/torch.dir/random.c.i

CMakeFiles/torch.dir/random.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/random.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/build/random.c -o CMakeFiles/torch.dir/random.c.s

CMakeFiles/torch.dir/random.c.o.requires:

.PHONY : CMakeFiles/torch.dir/random.c.o.requires

CMakeFiles/torch.dir/random.c.o.provides: CMakeFiles/torch.dir/random.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/random.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/random.c.o.provides

CMakeFiles/torch.dir/random.c.o.provides.build: CMakeFiles/torch.dir/random.c.o


CMakeFiles/torch.dir/Generator.c.o: CMakeFiles/torch.dir/flags.make
CMakeFiles/torch.dir/Generator.c.o: ../Generator.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_15) "Building C object CMakeFiles/torch.dir/Generator.c.o"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/torch.dir/Generator.c.o   -c /data/s1531565/torch/pkg/torch/Generator.c

CMakeFiles/torch.dir/Generator.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/torch.dir/Generator.c.i"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /data/s1531565/torch/pkg/torch/Generator.c > CMakeFiles/torch.dir/Generator.c.i

CMakeFiles/torch.dir/Generator.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/torch.dir/Generator.c.s"
	/software/software/GCCcore/4.9.3/bin/cc  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /data/s1531565/torch/pkg/torch/Generator.c -o CMakeFiles/torch.dir/Generator.c.s

CMakeFiles/torch.dir/Generator.c.o.requires:

.PHONY : CMakeFiles/torch.dir/Generator.c.o.requires

CMakeFiles/torch.dir/Generator.c.o.provides: CMakeFiles/torch.dir/Generator.c.o.requires
	$(MAKE) -f CMakeFiles/torch.dir/build.make CMakeFiles/torch.dir/Generator.c.o.provides.build
.PHONY : CMakeFiles/torch.dir/Generator.c.o.provides

CMakeFiles/torch.dir/Generator.c.o.provides.build: CMakeFiles/torch.dir/Generator.c.o


# Object files for target torch
torch_OBJECTS = \
"CMakeFiles/torch.dir/DiskFile.c.o" \
"CMakeFiles/torch.dir/File.c.o" \
"CMakeFiles/torch.dir/MemoryFile.c.o" \
"CMakeFiles/torch.dir/PipeFile.c.o" \
"CMakeFiles/torch.dir/Storage.c.o" \
"CMakeFiles/torch.dir/Tensor.c.o" \
"CMakeFiles/torch.dir/Timer.c.o" \
"CMakeFiles/torch.dir/utils.c.o" \
"CMakeFiles/torch.dir/init.c.o" \
"CMakeFiles/torch.dir/TensorOperator.c.o" \
"CMakeFiles/torch.dir/TensorMath.c.o" \
"CMakeFiles/torch.dir/random.c.o" \
"CMakeFiles/torch.dir/Generator.c.o"

# External object files for target torch
torch_EXTERNAL_OBJECTS =

libtorch.so: CMakeFiles/torch.dir/DiskFile.c.o
libtorch.so: CMakeFiles/torch.dir/File.c.o
libtorch.so: CMakeFiles/torch.dir/MemoryFile.c.o
libtorch.so: CMakeFiles/torch.dir/PipeFile.c.o
libtorch.so: CMakeFiles/torch.dir/Storage.c.o
libtorch.so: CMakeFiles/torch.dir/Tensor.c.o
libtorch.so: CMakeFiles/torch.dir/Timer.c.o
libtorch.so: CMakeFiles/torch.dir/utils.c.o
libtorch.so: CMakeFiles/torch.dir/init.c.o
libtorch.so: CMakeFiles/torch.dir/TensorOperator.c.o
libtorch.so: CMakeFiles/torch.dir/TensorMath.c.o
libtorch.so: CMakeFiles/torch.dir/random.c.o
libtorch.so: CMakeFiles/torch.dir/Generator.c.o
libtorch.so: CMakeFiles/torch.dir/build.make
libtorch.so: lib/luaT/libluaT.so.0
libtorch.so: lib/TH/libTH.so.0
libtorch.so: /software/software/OpenBLAS/0.2.15-GCC-4.9.3-2.25-LAPACK-3.6.0/lib/libopenblas.so
libtorch.so: CMakeFiles/torch.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/data/s1531565/torch/pkg/torch/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_16) "Linking C shared module libtorch.so"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/torch.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/torch.dir/build: libtorch.so

.PHONY : CMakeFiles/torch.dir/build

CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/DiskFile.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/File.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/MemoryFile.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/PipeFile.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/Storage.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/Tensor.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/Timer.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/utils.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/init.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/TensorOperator.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/TensorMath.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/random.c.o.requires
CMakeFiles/torch.dir/requires: CMakeFiles/torch.dir/Generator.c.o.requires

.PHONY : CMakeFiles/torch.dir/requires

CMakeFiles/torch.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/torch.dir/cmake_clean.cmake
.PHONY : CMakeFiles/torch.dir/clean

CMakeFiles/torch.dir/depend: TensorMath.c
CMakeFiles/torch.dir/depend: random.c
	cd /data/s1531565/torch/pkg/torch/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /data/s1531565/torch/pkg/torch /data/s1531565/torch/pkg/torch /data/s1531565/torch/pkg/torch/build /data/s1531565/torch/pkg/torch/build /data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/torch.dir/depend

