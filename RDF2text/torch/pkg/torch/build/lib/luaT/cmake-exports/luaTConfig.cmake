# Find the luaT includes and library
#
# LUAT_INCLUDE_DIR -- where to find the includes
# LUAT_LIBRARIES -- list of libraries to link against
# LUAT_FOUND -- set to 1 if found

SET(LUAT_FOUND 1)
SET(LUAT_INCLUDE_DIR "/data/s1531565/torch/install/include")
SET(LUAT_LIBRARIES "/data/s1531565/torch/install/lib/libluaT.so")
