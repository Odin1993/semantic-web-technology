l = open("dev.ref1", 'r', encoding='utf-8').readlines()
#arjan-predictions.relex
#dev.ref1

d = {}

totaal = 0

for team in l:
	# if we have not seen team before, create k/v pairing
	# setting value to 0, if team already in dict this does nothing
	d.setdefault(team,0)
	# increase the count for the team
	d[team] += 1
for team, count in d.items():
	if count > 1:
		print("{} {}".format(team,count))
		totaal += 1

print("TOTAAL: ", totaal)